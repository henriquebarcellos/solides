<?php
//Template Name: Contato
?>
<?php get_header(); ?>
		<section class="container contato">
			<h2 class="subtitulo">Contato</h2>

			<div class="grid-16">
				<a href="https://www.google.com.br/maps" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rest-mapa.jpg" alt="Fachada do Rest"></a>
			</div>

			<div class="grid-1-3 contato-item">
				<h2>Dados</h2>
				<p>31 3333-4444</p>
				<p>contato@rest.com</p>
				<p>facebook.com/rest/</p>
			</div>
			<div class="grid-1-3 contato-item">
				<h2>Horários</h2>
				<p>Segunda à Sexta: 10 às 23</p>
				<p>Sábado: 14 às 23</p>
				<p>Domingo: 14 às 22</p>
			</div>
			<div class="grid-1-3 contato-item">
				<h2>Endereço</h2>
				<p>Rua Maranhão, 29</p>
				<p>Belo Horizonte - MG</p>
				<p>Brasil</p>
			</div>
		</section>
<?php get_footer(); ?>