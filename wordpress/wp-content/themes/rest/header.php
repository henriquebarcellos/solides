<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<title><?php bloginfo('name'); ?></title>

		<link href='https://fonts.googleapis.com/css?family=Alegreya+SC' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css">

		<!-- HEADER WORDPRESS -->
			<?php wp_head(); ?>
		<!-- FECHA HEADER WORDPRESS -->

	</head>

	<body>
		<?php 
		
		$url = $_SERVER['REQUEST_URI'];
		$links = explode("/", $url);
		$pagina = $links[3];
		
		?>
		<header>
			<nav>
				<ul>
					<li <?php echo ($pagina == "" ? "class=\"current_page_item\"" : ""); ?>><a href="/solides/wordpress/">Menu</a></li>
					<li <?php echo ($pagina == "sobre" ? "class=\"current_page_item\"" : ""); ?>><a href="/solides/wordpress/sobre/">Sobre</a></li>
					<li <?php echo ($pagina == "contato" ? "class=\"current_page_item\"" : ""); ?>><a href="/solides/wordpress/contato/">Contato</a></li>
				</ul>
			</nav>

			<h1><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/rest.png" alt="Rest"></h1>

			<p>Rua Rio Grande do Norte 888 - Funcionários</p>
			<p class="telefone">3333-4444</p>
		</header>