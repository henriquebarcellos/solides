const baseUrl = $('#base-url').val() + '/index.php/';

var formUserValidate = function() {
    $("#editar-perfil-form").validate({
        rules: {         
            name: {required: true, minlength: 6},
            password: {required: true, minlength: 6, maxlength: 15},
            confirmPassword: {required: true, minlength: 6, maxlength: 15},
            company_id: {required: true}
        },
        messages: {
            'confirmPassword': {
                'equalTo': 'Senha não confere'
            },
            'name': {
                'equalTo': 'Insira seu nome'
            },                      
        },         
    });     
};

var signUpValidate = function(){
    $("#signup-form-submit").validate({
        rules: {
            name: {required: true, minlength: 6},
            password: {required: true, minlength: 6, maxlength: 15},
            confirmPassword: {required: true, minlength: 6, maxlength: 15},
            company_id: {required: true}
        },
        messages: {
            'confirmPassword': {
                'equalTo': 'Senha não confere'
            },
            'name': {
                'equalTo': 'Insira seu nome'
            },                      
        },         
    });   
};  

var changePasswordValidation = function() {
    $("#alterar-senha-form").validate({
        rules: {
            "actual-password": {required: true},
            "new-password": {required: true, minlength: 6, maxlength: 15},
            "confirm-new-password": {required: true, minlength: 6, maxlength: 15, equalTo: "#new-password"}

        },
        messages: {
            'confirm-new-password': {
                'equalTo': 'Senha não confere'
            }                  
        },         
    });     
};

var loginValidate = function() {
    $("#login-form-submit").validate({
        rules: {
            nameLogin: { required: true},          
            passwordLogin: {required: true, minlength: 6, maxlength: 15}
        },        
    });     
};      

var formUserSignUp = function() {
    $('.c-btn-signup').click(function(){
        $('#signup-form').modal();
        $('.signup-modal-body').html(
            '<div id="mensagem"></div>'+
            '<form action="" id="signup-form-submit" method="post" accept-charset="utf-8">'+                
                '<div class="form-group col-md-12">'+
                    '<label for="name">Nome*</label>'+
                    '<input type="text" name="name" value="" id="name" class="form-control c-square" maxlength="255" minlength="6">'+
                '</div>'+
                '<div class="clearfix"></div>'+                
                '<div class="form-group col-md-6">'+
                    '<label for="password">Senha*</label>'+
                    '<input type="password" name="password" value="" id="password" class="form-control c-square" maxlength="15" minlength="6">'+
                '</div>'+
                '<div class="form-group col-md-6">'+
                    '<label for="confirmPassword">Confirme sua senha*</label>'+
                    '<input type="password" name="confirmPassword" value="" id="confirmPassword" class="form-control c-square" maxlength="15" minlength="6">'+
                '</div>'+
                '<div class="clearfix"></div>'+
                '<div class="form-group col-md-12">'+
                    '<label for="company">Empresa*</label>'+
                    '<select name="company_id" id="company" class="form-control c-square c-theme company">'+
                        '<option value="0"></option>'+
                    '</select>'+
                '</div>'+                
                '<div class="clearfix"></div>'+
                '<div class="form-group col-md-6">'+
                    '<button type="text" id="form-user-signup-submit" name="form-user-signup-submit" class="btn c-theme-btn btn-lg c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Cadastrar</button>'+
                '</div>'+
                '<div class="form-group col-md-6">'+    
                    '<a href="javascript:;" class="c-btn-forgot c-pull-right c-margin-t-10" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Voltar para a tela de login</a>'+
                '</div>'+
                '<div class="clearfix"></div>'+
            '</form>');
            $('.signup-modal-body .company').appendTo(
                $.getJSON(baseUrl + 'Companies/loadCompanies', function(dados){

                var options;
                options = '<option value="0"></option>';
                for (var i = 0; i < dados.length; i++) {
                    options += '<option value="' + dados[i].id + '" >' + dados[i].name + '</option>';
                }   
                $('.company').html(options).show();
                })  
            );                        
    });
};

var formUserSignUpSubmit = function() {
    $(document).on('click', '#form-user-signup-submit', function(){        
        if ($('#signup-form-submit').valid()) {
            $.ajax({
                beforeSend: function() { $('.loading').show(); },
                url: baseUrl + 'Users/formUserSignUpSubmit',
                type: 'POST',
                data: $("#signup-form-submit").serialize(),
                complete: function() { $('.loading').hide(); },
                success: function(result){
                    if(result == 'submit-success'){
                        $('#signup-form').modal('toggle');
                        $('#success-modal .alert.alert-success').html('Cadastro realizado com sucesso!');
                        $('#success-modal').modal();
                        location.reload();                        
                    }else if (result == 'registered-name'){
                        $('#error-modal .alert.alert-danger').html('Nome já cadastrado!');
                        $('#error-modal').modal();                                
                    }
                    else {
                        $('#error-modal .alert.alert-danger').html('Erro ao realizar cadastro, tente novamente...<br/>' + result);
                        $('#error-modal').modal();                                
                    }
                }
            });
            return false;
        }
    }); 
};


var formUserLogin = function() {
    $("#form-user-login-submit").click(function(){        
        if ($('#login-form-submit').valid()) {
            $.ajax({
                beforeSend: function() { $('.loading').show(); },
                url: baseUrl + 'users/formUserLoginSubmit',
                type: 'POST',
                data: $("#login-form-submit").serialize(),
                complete: function() { $('.loading').hide(); },
                success: function(result){
                    if(result == 'Logado com sucesso!'){
                        $('#login-form').modal('toggle');
                        $('#success-modal .alert.alert-success').html(result);
                        $('#success-modal').modal();
                        location.reload();                        
                    }else {
                        $('#login-form').modal('toggle');
                        $('#error-modal .alert.alert-danger').html(result);
                        $('#error-modal').modal();                                
                    }
                }
            });
            return false;
        }
    }); 
};

var formUserLogout = function() {
    $('.userLogout').click(function(){
        $.ajax({
            beforeSend: function() { $('.loading').show(); },
            url: baseUrl + 'users/formUserLogout',
            type: 'POST',
            data: $(".form-user-logout-submit").serialize(),
            complete: function() { $('.loading').hide(); },
            success: function(result){
                if(result == 'success'){
                    $('#success-modal .alert.alert-success').html('Deslogado com sucesso!');
                    $('#success-modal').modal();
                    location.reload();
                }else {
                    $('#error-modal .alert.alert-danger').html('Erro ao realizar log out!');
                    $('#error-modal').modal();   
                }
            }
        });
        return false;
    }); 
};

var recordStart = function () {
    $(document).on('click', '.record-start', function(){ 
        var element = document.getElementById("record-start");
        var elementNext = document.getElementById("record-lunch-start");
          
        $.getJSON(baseUrl + 'Records/insertStart', function(result){  
            if (result == 'success') {
                element.classList.add("disabled");
                element.classList.add("btn-success");
                element.classList.remove("c-theme-btn");
                elementNext.classList.remove("disabled");
            } else {
                $('#error-modal .alert.alert-danger').html('Erro ao atualizar histórico!');
                $('#error-modal').modal();
            }                                              
        });
    });
};

var recordLunchStart = function () {
    $(document).on('click', '.record-lunch-start', function(){ 
        var element = document.getElementById("record-lunch-start");
        var elementNext = document.getElementById("record-lunch-end");
        var id = document.getElementById("id-record").value;
        var type = "lunch_start";
        var dataString = "idRecord=" + id + "&type=" + type;        
        
        $.ajax({
            url: baseUrl + 'Records/updateRecord',
            type: 'POST',
            data: dataString,
            success: function(result){
                if (result == 'success') {
                    element.classList.add("disabled");
                    element.classList.add("btn-success");
                    element.classList.remove("c-theme-btn");
                    elementNext.classList.remove("disabled");
                } else if (result == 'invalid id') {
                    $('#error-modal .alert.alert-danger').html('Erro ao encontrar o registro!');
                    $('#error-modal').modal();
                } else {
                    $('#error-modal .alert.alert-danger').html('Erro ao atualizar histórico!');
                    $('#error-modal').modal();
                } 
            }
        });       
        return false;
    });
};

var recordLunchEnd = function () {
    $(document).on('click', '.record-lunch-end', function(){ 
        var element = document.getElementById("record-lunch-end");
        var elementNext = document.getElementById("record-end");
        var id = document.getElementById("id-record").value;
        var type = "lunch_end";
        var dataString = "idRecord=" + id + "&type=" + type;        
        
        $.ajax({
            url: baseUrl + 'Records/updateRecord',
            type: 'POST',
            data: dataString,
            success: function(result){
                if (result == 'success') {
                    element.classList.add("disabled");
                    element.classList.add("btn-success");
                    element.classList.remove("c-theme-btn");
                    elementNext.classList.remove("disabled");
                } else if (result == 'invalid id') {
                    $('#error-modal .alert.alert-danger').html('Erro ao encontrar o registro!');
                    $('#error-modal').modal();
                } else {
                    $('#error-modal .alert.alert-danger').html('Erro ao atualizar histórico!');
                    $('#error-modal').modal();
                } 
            }
        });       
        return false;
    });
};

var recordEnd = function () {
    $(document).on('click', '.record-end', function(){ 
        var element = document.getElementById("record-end");
        var id = document.getElementById("id-record").value;
        var type = "end";
        var dataString = "idRecord=" + id + "&type=" + type;        
        
        $.ajax({
            url: baseUrl + 'Records/updateRecord',
            type: 'POST',
            data: dataString,
            success: function(result){
                if (result == 'success') {
                    $('#success-modal .alert.alert-success').html('Seu expediente de hoje acabou, até amanhã!');
                    $('#success-modal').modal();
                    
                    $('#success-modal').on('shown.bs.modal', function() {
                        var $me = $(this);

                        $me.delay(2000).hide(0, function() {
                            $me.modal('hide');
                        });
                    });
                    element.classList.add("disabled");
                    element.classList.add("btn-success");
                    element.classList.remove("c-theme-btn");
                } else if (result == 'invalid id') {
                    $('#error-modal .alert.alert-danger').html('Erro ao encontrar o registro!');
                    $('#error-modal').modal();
                } else {
                    $('#error-modal .alert.alert-danger').html('Erro ao atualizar histórico!');
                    $('#error-modal').modal();
                } 
            }
        });       
        return false;
    });
};

var setInputFocusModal = function() {
    $('#signup-form').on('shown.bs.modal', function() {
        $('#name').focus();
    });

    $('#login-form').on('shown.bs.modal', function() {
        $('#nameLogin').focus();
    });
}

var setInputFocus = function() {
    $('#alterar-senha-actual-password').focus();
    $('#editar-perfil-name').focus();
    $('#login-email').focus();
}

$(document).ready(function () {
    formUserValidate();
    signUpValidate();
    changePasswordValidation();
    loginValidate();           
    formUserSignUp();
    formUserSignUpSubmit();
    formUserLogin();  
    formUserLogout();
    recordStart();
    recordLunchStart();
    recordLunchEnd();
    recordEnd();
    setInputFocusModal();
    setInputFocus();
});