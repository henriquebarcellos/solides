<?php
    
function checkEmail($email) 
{        
    $format = '/[a-z0-9_\.\-]+@[a-z0-9_\.\-]*[a-z0-9_\.\-]+\.[a-z]{2,4}$/';

    if(preg_match($format, $email)) {
        return true;
    } else {
        return false;
    }
}

function checkData($Data) {
    $format = explode(' ', $Data); //separa data do tempo
    $data = explode('-', $format[0]); //índice 0 é a DATA e 1 é o TEMPO

    //índice 2 é o ano, 1 é o mês e 0 o dia
    $data = $data[2] . '/' . $data[1] . '/' . $data[0];

    return $data;
}

function checkTime($Data) {
    $format = explode(' ', $Data); //separa data do tempo
    $data = explode('-', $format[0]); //índice 0 é a DATA e 1 é o TEMPO

    return $format[1];
}

?>
