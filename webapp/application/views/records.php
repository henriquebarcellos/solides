<div class="c-layout-page">
    <div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Histórico</h3>
            </div>            
        </div>
    </div>
    <!-- BEGIN: PAGE CONTENT -->
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-content-title-1">
                <h3 class="c-center c-font-dark c-font-uppercase">Histórico</h3>
                <div class="c-line-center c-theme-bg"></div>
                <p class="c-center">Histórico de apontamentos do Usuário</p>
            </div>
            
            <?php 
            if(empty($records)) {
                echo '<div>'.
                        '<div class="alert alert-info" role="alert" style="margin-top: 20px;margin-bottom: 10px; padding: 30px;text-align: center;">Este usuário não possui ainda nenhum histórico de ponto.</div>'.
                    '</div>';
            } else {            
            ?>
            
            <div class="c-content-panel">                
                <div class="c-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Data</th>
                                        <th>Chegada</th>
                                        <th>Almoço - Início</th>
                                        <th>Almoço - Fim</th>
                                        <th>Saída</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($records as $record) {
                                    
                                        echo "<tr>
                                            <th scope=\"row\">{$record['id']}</th>
                                            <td>" . checkData($record['date_start']) . "</td>
                                            <td>" . checkTime($record['date_start']) . "</td>
                                            <td>" . (isset($record['date_lunch_start']) ? checkTime($record['date_lunch_start']) : "") ."</td>
                                            <td>" . (isset($record['date_lunch_end']) ? checkTime($record['date_lunch_end']) : "") ."</td>
                                            <td>" . (isset($record['date_end']) ? checkTime($record['date_lunch_end']) : "") ."</td>
                                        </tr>";
                                    
                                    }
                                    ?>                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php
            }
            ?>
        </div>
    </div>
    <!-- END: PAGE CONTENT -->
</div>