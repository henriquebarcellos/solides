<!-- BEGIN: PAGE CONTAINER -->
<div class="c-layout-page">
    <!-- BEGIN: PAGE CONTENT -->    
        
    <section id="records">
        <div class="c-content-box c-size-md c-margin-t-60">
            <div class="container">
                <div class="c-content-title-1">
                    <h3 class="c-center c-font-uppercase c-font-bold">Registre o seu ponto</h3>
                    <div class="c-line-center"></div>
                </div>
                <?php
                if(!$this->session->userdata('logged_user')) {
                    echo '<div>'.
                        '<div class="alert alert-info" role="alert" style="margin-top: 20px;margin-bottom: 10px; padding: 30px;text-align: center;">Faça o login primeiro!</div>'.
                    '</div>';
                } else {
                    
                    $userSession = $this->session->get_userdata();                                                
                    $records = $this->record_model->getRecordUserByDate($userSession['logged_user'][0]['id'], $userSession['logged_user']['0']['company_id']);
                    $recordMaxId = $this->record_model->getMaxId();
                    
                    if(!empty($records[0]['date_end'])) {    
                        echo '<div class="alert alert-info" role="alert">
                                Seu expediente de hoje acabou, até amanhã!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                            </div>';
                        }                    
                ?>
                
                <div class="c-margin-t-40">                    
                    <div class="row">
                        <input type="hidden" id="id-record" name="id-record" value="<?php echo (isset($records[0]['id']) ? $records[0]['id'] : (isset($recordMaxId) ? $recordMaxId+1 : 1)); ?>">
                        <div class="col-md-3 col-sm-12 col-xs-12 c-margin-b-20">
                            <a href="javascript:;" id="record-start" class="btn btn-xlg c-btn-square c-btn-uppercase c-btn-bold <?php echo (empty($records[0]['date_start']) ? "c-theme-btn" : "btn-success disabled"); ?> record-start">Chegada</a>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 c-margin-b-20">
                            <a href="javascript:;" id="record-lunch-start" class="btn btn-xlg c-btn-square c-btn-uppercase c-btn-bold <?php echo (!empty($records[0]['date_lunch_start']) ? "btn-success disabled" : (!empty($records[0]['date_start']) ? "c-theme-btn" : "c-theme-btn disabled") ); ?> record-lunch-start">Almoço - Início</a>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 c-margin-b-20 text-right">
                            <a href="javascript:;" id="record-lunch-end" class="btn btn-xlg c-btn-square c-btn-uppercase c-btn-bold <?php echo (!empty($records[0]['date_lunch_end']) ? "btn-success disabled" : (!empty($records[0]['date_lunch_start']) ? "c-theme-btn" : "c-theme-btn disabled") ); ?> record-lunch-end">Almoço - Fim</a>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 c-margin-b-20 text-right">
                            <a href="javascript:;" id="record-end" class="btn btn-xlg c-btn-square c-btn-uppercase c-btn-bold <?php echo (!empty($records[0]['date_end']) ? "btn-success disabled" : (!empty($records[0]['date_lunch_end']) ? "c-theme-btn" : "c-theme-btn disabled") ); ?> record-end">Saída</a>
                        </div>
                    </div>                                     
                </div>
                <?php
                    }
                ?>
            </div>
        </div>
    </section>     
    
    <section id="stats">
        <div class="c-content-box c-size-md c-bg-white">
            <div class="container">
                <div class="c-content-counter-1 c-opt-1">
                    <div class="c-content-title-1">
                        <h3 class="c-center c-font-uppercase c-font-bold">Vejamos os números!</h3>
                        <div class="c-line-center"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="c-counter c-theme-border c-font-bold c-theme-font" data-counter="counterup"><?php echo $statistics['users'] ?></div>
                            <h4 class="c-title c-first c-font-uppercase c-font-bold">Usuários cadastrados</h4>
                            <p class="c-content">Quantidade de usuários cadastrados.</p>
                        </div>
                        <div class="col-md-4">
                            <div class="c-counter c-theme-border c-font-bold c-theme-font" data-counter="counterup"><?php echo $statistics['companies'] ?></div>
                            <h4 class="c-title c-font-uppercase c-font-bold">Empresas cadastradas</h4>
                            <p class="c-content">Quantidade de empresas cadastradas.</p>
                        </div>
                        <div class="col-md-4">
                            <div class="c-counter c-theme-border c-font-bold c-theme-font" data-counter="counterup"><?php echo $statistics['records'] ?></div>
                            <h4 class="c-title c-font-uppercase c-font-bold">Total de registros</h4>
                            <p class="c-content">Quantidade total de registros contabilizados no app.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- END: PAGE CONTENT -->
</div>
<!-- END: PAGE CONTAINER -->
