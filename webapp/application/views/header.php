<!DOCTYPE html>
<html lang="pt-br">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>Solides - Gestão de ponto eletrônico</title>
        <base href="<?php echo base_url(); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta http-equiv="Content-type" content="text/html; charset=utf-8">
        
        
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700&amp;subset=all' rel='stylesheet' type='text/css'>
        <link href="assets/plugins/socicon/socicon.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/bootstrap-social/bootstrap-social.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/animate/animate.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="assets/base/css/plugins.css" rel="stylesheet" type="text/css" />
        <link href="assets/base/css/components.css" id="style_components" rel="stylesheet" type="text/css" />
        <link href="assets/base/css/themes/default.css" rel="stylesheet" id="style_theme" type="text/css" />
        <link href="assets/base/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="assets/base/img/content/favicon/favicon.png" /> </head>
     
    <body class="c-layout-header-fixed c-layout-header-mobile-fixed c-layout-header-fullscreen">
        <input type="hidden" name="base-url" id="base-url" value="<?php echo base_url() ?>">
        <div class="loading modal-backdrop fade in" style="z-index: 99999; display: none;"></div>
        <div class="loading" style="position: absolute;z-index: 99999;width: 60px;right: 50%;top: 48%; display: none;">
            <img src="assets/base/img/content/loading/loading.gif" align="middle" alt="Carregando informações">&nbsp;
            <p class="c-font-white" style="margin: 10px -12px; ">Carregando...</p>
        </div>
        <!-- BEGIN: LAYOUT/HEADERS/HEADER-ONEPAGE -->
        <!-- BEGIN: HEADER -->
        <header class="c-layout-header c-layout-header-onepage c-layout-header-default c-layout-header-dark-mobile c-header-transparent-dark" id="home" data-minimize-offset="0">
            <div class="c-navbar">
                <div class="container-fluid">
                    <!-- BEGIN: BRAND -->
                    <div class="c-navbar-wrapper clearfix">
                        <div class="c-brand c-pull-left">
                            <a href="index.php" class="c-logo">
                                <img src="assets/base/img/layout/logos/logo-1.png" alt="WebApp" class="c-desktop-logo">
                                <img src="assets/base/img/layout/logos/logo-1.png" alt="WebApp" class="c-desktop-logo-inverse">
                                <img src="assets/base/img/layout/logos/logo-1.png" alt="WebApp" class="c-mobile-logo"> </a>
                            <button class="c-hor-nav-toggler" type="button" data-target=".c-mega-menu">
                                <span class="c-line"></span>
                                <span class="c-line"></span>
                                <span class="c-line"></span>
                            </button>
                            
                        </div>
                        <!-- END: BRAND -->
                        
                        <!-- BEGIN: HOR NAV -->
                        <!-- BEGIN: LAYOUT/HEADERS/MEGA-MENU-ONEPAGE -->
                        <!-- BEGIN: MEGA MENU -->
                        <nav class="c-mega-menu c-mega-menu-onepage c-pull-right c-mega-menu-dark c-mega-menu-dark-mobile c-fonts-uppercase c-fonts-bold" data-onepage-animation-speed="700">
                            <ul class="nav navbar-nav c-theme-nav">
                                <li class="c-onepage-link c-active active">
                                    <a href="<?php echo isset($_SERVER['PATH_INFO']) ? "index.php" : "#home" ?>"  
                                    class="c-link">Início</a>
                                </li>
                                <li class="c-onepage-link ">
                                    <a href="index.php/records" class="c-link">Históricos</a>
                                </li>
                                <li class="c-onepage-link ">
                                    <a href="#stats" class="c-link">Estatísticas</a>
                                </li>
                                
                                <?php 
                                if(!$this->session->userdata('logged_user')){ 
                                    echo '
                                    <li>
                                        <a href="javascript:;" data-toggle="modal" data-target="#login-form" class="c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-border-1x c-btn-white c-btn-circle c-btn-uppercase c-btn-sbold">
                                            <i class="icon-user"></i> Entrar</a>
                                    </li>';              
                                }else{
                                    $userName = explode(' ', $this->session->userdata('logged_user')[0]['name']);
                                    $helloUsernameMsg = substr('olá, '.$userName[0], 0, 26);
                                    $result = 28 - strlen($helloUsernameMsg);
                                    $leftRightPx = $result * 4;
                                    echo '
                                    <li>
                                        <a style="padding: 6px '.$leftRightPx.'px 4px '.$leftRightPx.'px !important;" href="javascript:;" data-toggle="modal" data-target="#account-info-form" class="c-btn-sign-in c-btn-border-opacity-04 c-btn btn-no-focus c-btn-header btn btn-sm c-btn-dark c-btn-circle c-btn-uppercase c-btn-sbold">
                                            <i class="icon-user"></i>'.$helloUsernameMsg.'</a>
                                    </li>'; 
                                } 
                                ?>                                 
                            </ul>
                        </nav>
                        <!-- END: MEGA MENU -->
                        <!-- END: LAYOUT/HEADERS/MEGA-MENU-ONEPAGE -->
                        <!-- END: HOR NAV -->
                    </div>
                </div>
            </div>
        </header>
        <!-- END: HEADER -->
        <!-- END: LAYOUT/HEADERS/HEADER-ONEPAGE -->
        
        <!-- BEGIN: CONTENT/USER/account-info-FORM -->
        <div class="modal fade c-content-login-form" id="account-info-form" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content c-square">
                    <div class="modal-header c-no-border">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        
                        <?php 
                        echo form_open('', 'id="form-user-logout-submit"');
                        echo 
                        '<div class="form-group">
                            <a href="index.php/alterar-senha" class="btn btn-primary btn-block">Alterar senha</a>
                        </div>
                        <div class="form-group">
                            <a href="javascript:;" class="btn btn-danger btn-block userLogout">Log out</a>            
                        </div>';                                                

                        echo form_close();
                        ?>

                    </div>
                    <div class="modal-footer c-no-border">                  
        
                    </div>
                </div>
            </div>
        </div>
        <!-- END: CONTENT/USER/account-info-FORM --> 

        <!-- BEGIN: CONTENT/USER/success modal -->
        <div class="modal fade bs-example-modal-sm" id="success-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; z-index: 10999">
            <div class="modal-dialog modal-sm">
                <div class="modal-content c-square">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="mySmallModalLabelSuccess">Sucesso!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" role="alert">Sucesso.</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn c-theme-btn c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- END: CONTENT/USER/success modal -->    

        <!-- BEGIN: CONTENT/USER/error modal -->
        <div class="modal fade bs-example-modal-sm" id="error-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; z-index: 10999">
            <div class="modal-dialog modal-sm">
                <div class="modal-content c-square">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="mySmallModalLabelError">Erro!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-danger" role="alert">Ops!</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn c-theme-btn c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- END: CONTENT/USER/error modal -->              


        <!-- BEGIN: CONTENT/USER/error modal -->
        <div class="modal fade bs-example-modal-sm" id="warning-modal" tabindex="-1" role="dialog" aria-hidden="true" style="display: none; z-index: 10999">
            <div class="modal-dialog modal-sm">
                <div class="modal-content c-square">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h4 class="modal-title" id="mySmallModalLabelWarning">Atenção!</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-warning" role="alert">Ops!</div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn c-theme-btn c-btn-border-2x c-btn-square c-btn-bold c-btn-uppercase" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- END: CONTENT/USER/error modal -->          

        <!-- BEGIN: CONTENT/USER/SIGNUP-FORM -->
        <div class="modal fade" id="signup-form" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content c-square">
                    <div class="modal-header c-no-border">                                    
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="c-font-24 c-font-sbold">Crie sua conta</h3>      
                        <p>Preencha o formulário abaixo para se cadastrar</p>                          
                    </div>
                    <div class="modal-body signup-modal-body">
                        <div id="mensagem"></div>
                        <?php 

                            echo form_open("", 'id="signup-form-submit"');
                           
                            echo '<div class="form-group col-md-12">';
                            echo form_label("Nome*", "name");
                            echo form_input(array(
                                "name" => "name",
                                "id" => "name",
                                "class" => "form-control c-square",
                                "maxlength" => "255",
                                "minlength" => "6"
                            ));
                            echo '</div>';       
                            echo '<div class="clearfix"></div>';           
                            echo '<div class="form-group col-md-6">';          
                            echo form_label("Senha*", "password");
                            echo form_password(array(
                                "name" => "password",
                                "id" => "password",
                                "class" => "form-control c-square",
                                "maxlength" => "15",
                                "minlength" => "6"
                            ));
                            echo '</div>';
                            echo '<div class="form-group col-md-6">';
                            echo form_label("Confirme sua senha*", "confirmPassword");
                            echo form_password(array(
                                "name" => "confirmPassword",
                                "id" => "confirmPassword",
                                "class" => "form-control c-square",
                                "maxlength" => "15",
                                "minlength" => "6"
                            ));
                            echo '</div>';     
                            echo '<div class="clearfix"></div>';                            

                            echo '<div class="form-group col-md-12">';
                            
                            $companies = $this->company_model->getCompanies();
                            foreach ($companies as $key) {
                                $company[$key['id']] = $key['name'];
                            }
                            
                            echo form_label("Empresa*", "company");
                            echo form_dropdown(
                                 'company_id', $company, '', 'id="company" class="form-control c-square c-theme c-height-46 company"'
                            );
                            echo '</div>';

                            echo '<div class="clearfix"></div>';                       
                                                                                       
                            echo '<div class="form-group">
                                <button type="submit" id="form-user-signup-submit" name="form-user-signup-submit" value="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Cadastrar</button>
                                <a href="javascript:;" class="c-btn-forgot" data-toggle="modal" data-target="#login-form" data-dismiss="modal">Voltar para a tela de login</a>
                                </div>';                                                                                                                  
                            echo '<div class="clearfix"></div>';                      

                            echo form_close();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: CONTENT/USER/SIGNUP-FORM -->

        <!-- BEGIN: CONTENT/USER/LOGIN-FORM -->
        <div class="modal fade c-content-login-form" id="login-form" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content c-square">
                    <div class="modal-header c-no-border">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3 class="c-font-24 c-font-sbold">Identificação</h3>
                        
                        <?php 
                        echo form_open('', 'id="login-form-submit"');
                        echo '<div class="form-group">';
                        echo form_label("Nome", "nameLogin");
                        echo form_input(array(
                            "name" => "nameLogin",
                            "id" => "nameLogin",
                            "class" => "form-control c-square",
                            "minlength" => "6",
                            "maxlength" => "255",
                            "placeholder" => 'Nome'
                        ));                        
                        echo  '</div>';

                        echo '<div class="form-group">';
                        echo form_label("Senha", "passwordLogin");
                        echo form_password(array(
                            "name" => "passwordLogin",
                            "id" => "passwordLogin",
                            "class" => "form-control c-square",
                            "minlength" => "6",
                            "maxlength" => "15",                            
                            "placeholder" => 'Senha'
                        ));                        
                        echo  '</div>';

                        echo '<div class="form-group">
                                <div class="c-checkbox">
                                    <input type="checkbox" id="login-rememberme" class="c-check">
                                    <label for="login-rememberme" class="c-font-thin c-font-17"></label>
                                        <span></span>
                                        <span class="check"></span>                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" id="form-user-login-submit" name="form-user-login-submit" value="submit" class="btn c-theme-btn btn-md c-btn-uppercase c-btn-bold c-btn-square c-btn-login">Login</button>                                
                            </div>';


                        echo form_close();
                        ?>
                    </div>
                    <div class="modal-footer c-no-border">                  
                        <?php
                            echo form_open('', "id='form-cadastro-usuario'");
                            echo '<span class="c-text-account">Não possui uma conta ainda?</span>' ;
                            echo '<a href="javascript:;" data-dismiss="modal" class="btn c-btn-dark-1 btn c-btn-uppercase c-btn-bold c-btn-slim c-btn-border-2x c-btn-square c-btn-signup">Cadastrar!</a>';
                            echo form_input(array(
                            'type' => 'hidden',
                            'name' => 'form-cadastro-usuario',
                            'value' => 'submit',
                            ));
                            echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: CONTENT/USER/LOGIN-FORM -->