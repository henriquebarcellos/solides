<!-- BEGIN: PAGE CONTENT -->
<div class="c-layout-page">
    <div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-sbold">Alterar Senha</h3>
            </div>            
        </div>
    </div>
    <div class="c-content-box c-size-md c-bg-white">        
        <div class="container">                              
            <!-- BEGIN: PAGE CONTAINER -->
            <div class="c-content-title-1">                
                <h3 class="c-font-uppercase c-font-bold">Alterar Senha</h3>
                <div class="c-line-left"></div>
                <?php 
                    if ($validationStatus == 'success') {
                        echo '<div class="alert alert-success"> Perfil atualizado com sucesso! </div>';
                    }else if ($validationStatus == 'incorrect-password'){
                        echo '<div class="alert alert-danger"> A senha atual é incorreta! </div>';
                    } else if ($validationStatus == 'error-validation'){
                        echo '<div class="alert alert-danger"> Erro ao alterar senha! </div>';
                        echo $validationError;
                    }

                ?>
            </div>            
            <form  action="index.php/alterar-senha" class="c-shop-form-1" method="post" id="alterar-senha-form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="control-label">Senha atual</label>
                                <input name="actual-password" id="alterar-senha-actual-password" type="password" class="form-control c-square c-theme">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="control-label">Nova senha</label>
                                <input name="new-password" id="new-password" type="password" class="form-control c-square c-theme">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="control-label">Confirmar nova senha</label>
                                <input name="confirm-new-password" type="password" class="form-control c-square c-theme">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row c-margin-t-30">
                    <div class="form-group col-md-12" role="group">
                        <button type="submit" name="submit-alterar-senha-form" value="submit" class="btn btn-lg c-theme-btn c-btn-square c-btn-uppercase c-btn-bold">Confirmar</button>
                        <input type="hidden" name="user-id" value="<?php echo $user['0']['id'] ?>" >
                        <button type="submit" class="btn btn-lg btn-default c-btn-square c-btn-uppercase c-btn-bold">Cancelar</button>
                    </div>
                </div>
            </form>
            <!-- END: PAGE CONTAINER -->
        </div>
    </div>
</div>
<!-- END: PAGE CONTENT -->  