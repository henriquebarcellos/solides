<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_Model extends CI_Model {	
    
    public function createCompany($company)
    {
        $this->db->insert('companies', array(
            'name' => $company['name']              
        ));
    }

    public function getCompanies()
    {        
        $this->db->select('id, name');
        $this->db->from('companies');        
        return $this->db->get()->result_array();
    }
    
    public function getCompanyByID($companyId)
    {
        return $this->db->get_where('companies', array('id' => $companyId))->result_array();
    }
        
}
?>