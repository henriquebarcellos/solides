<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Record_Model extends CI_Model {	
    
    public function createRecord($record)
    {
        $this->db->insert('records', $record);        
        $recordId = $this->db->insert_id();
        
        return $recordId;
    }

    public function getRecords()
    {        
        $this->db->select('id, company_id, user_id, date_start, date_lunch_start, date_lunch_end, date_end');
        $this->db->from('records');
        return $this->db->get()->result_array();
    }
    
    public function getRecordUserByDate($userId, $companyId)
    {
        $dataAtual = date('Y-m-d');
        $this->db->select('*');
        $this->db->from('records');
        $this->db->where(array('user_id' => $userId, 'company_id' => $companyId));
        $this->db->like('date_start', $dataAtual, 'after');        
        return $this->db->get()->result_array();        
    }
    
    public function getRecordUserById($userId, $companyId)
    {        
        $this->db->select('*');
        $this->db->from('records');
        $this->db->where(array('user_id' => $userId, 'company_id' => $companyId));
        $this->db->order_by('date_start ASC');        
        return $this->db->get()->result_array();
    }
        
    public function setRecordByID($record, $field)
    {   
        if($field == "date_lunch_start") {
            return $this->db->update('records', array(
                'date_lunch_start' => $record['date_lunch_start']), array('id' => $record['id'])); 
        } else if($field == "date_lunch_end") {
            return $this->db->update('records', array(
                'date_lunch_end' => $record['date_lunch_end']), array('id' => $record['id'])); 
        } else {
            return $this->db->update('records', array(
                'date_end' => $record['date_end']), array('id' => $record['id'])); 
        }               
    }
    
    public function getMaxId() {
        $this->db->select_max('id');        
        $query = $this->db->get('records');
    }
}
?>