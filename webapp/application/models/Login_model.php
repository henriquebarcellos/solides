<?php

/**
 * Login [ MODEL ]
 * Responsável por autenticar, validar e checar usuário do sistema de Login!
 * 
 * @copyright (c) 2018, Henrique Barcellos
 */
class Login_Model extends CI_Model {
    
    private $Level;
    private $Email;
    private $Senha;   
    private $Result;
    
    /**
     * <b>Informar o Level:</b> Informe o nível de acesso mínimo para a área protegida do sisetma.
     * @param INT $Level = Nível mínimo para acesso
     */
    function __construct($Level) {
        $this->Level = (int) $Level;
    }
    
    /**
     * <b>Efetuar login:</b> ARRAY com dados de e-mail e senha do usuário para ser validado, e efetuar login
     * @param ARRAY $UserData = user [email, pass]
     */
    public function ExeLogin(array $UserData) {
        $this->Email = (string) strip_tags(trim($UserData['user']));
        $this->Senha = (string) strip_tags(trim($UserData['pass']));
        $this->setLogin();
    }
    
    /**
     * <b>Verificar Login:</b> Executando um getResult pode verificar se foi ou não efetuado o acesso com os dados.
     * @return BOOL $var = true para login e false para erro
     */
    public function getResult() {
        return $this->Result;
    }
        
    /**
     * <b>Checar Login:</b> Método para verificar se a sessão 'userlogin' e revalidar o acesso de telas restritas
     * @return BOOL $login = Retorna true ou mata a sessão retornando false!
     */
    public function CheckLogin() {
        if(empty($_SESSION['userlogin']) || $_SESSION['userlogin']['user_level'] < $this->Level) {
            unset($_SESSION['userlogin']);
            return false;
        } else {
            return true; 
        }
    }

    
    /*
     * ****************************************
     * ************ PRIVATE METHODS ***********
     * ****************************************
     */
    
    //Valida os dados e armazena os erros caso existam. Executa o login!
    private function setLogin() {
        //senão existir email, senha ou o e-mail for inválido, gerar msgErro
        if(!$this->Email || !$this->Senha || checkEmail($this->Email)) {
            $this->Error = ['Informe seu e-mail e senha para efetuar o login!'];
            $this->Result = false;
        } else if(!$this->getUser()) {
            $this->Error = ['Os dados informados não são compatíveis!'];
            $this->Result = false;
        } else if($this->Result['user_level'] < $this->Level) { //permite se level for maior ou igual ao permitido
            $this->Error = ["Desculpe {$this->Result['user_name']}, você não tem permissão para acessar esta área!"];
            $this->Result = false;
        } else {
            $this->Execute();
        }
    }
    
    //Verifica usuário e senha no banco de dados
    private function getUser() {
        $this->Senha = md5($this->Senha);
        
        $read = new Read;
        $read->ExeRead('ws_users', "WHERE user_email = :email AND user_password = :pass", "email={$this->Email}&pass={$this->Senha}");
        
        if($read->getResult()) {
            $this->Result = $read->getResult()[0]; //pega o primeiro registro, porque em tese só deve ter um com os dados
            return true;
        } else {
            return false;
        }
    }
        
    //Executa o login de fato. Starta sessão, caso ela ainda não foi iniciada!
    private function Execute() {
        if(!session_id()) {
            session_start();
        }
        
        $_SESSION['userlogin'] = $this->Result;
        $this->Error = ["Olá {$this->Result['user_name']}, seja bem-vindo(a). Aguarde redirecionamento!"];
        $this->Result = true;
    }
}
