<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_Model extends CI_Model {
    
    public function getCountRecords()
    {     
        return $this->db->count_all_results('records');
    }
    
    public function getCountUsers()
    {
        return $this->db->count_all_results('users');
    }
    
    public function getCountCompanies()
    {
        return $this->db->count_all_results('companies');
    }

}
?>