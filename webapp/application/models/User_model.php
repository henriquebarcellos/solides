<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {

    public function createUser($user)
    {
        $this->db->insert('users', $user);
        $userId = $this->db->insert_id();
        
        return $userId;
    }
    
    public function getUsers()
    {        
        $this->db->select('id, name');
        $this->db->from('users');        
        return $this->db->get()->result_array();
    }
    
    public function getUserByID($userId)
    {
        return $this->db->get_where('users', array('id' => $userId))->result_array();
    }
    
    public function getUserByName($userName)
    {
        return $this->db->get_where('users', array('name' => $userName))->result_array();
    }
    
    public function getUserByNameandPassword($user)
    {
        return $this->db->get_where('users', array('name' => $user['name'], 'password' => $user['password']))->result_array();
    }
    
    public function updatePasswordByName($user)
    {
        $this->db->where('name', $user['name']);
        $this->db->update('users', array('password' => $user['password']));
    }
}
?>