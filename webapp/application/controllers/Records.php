<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Records extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
            $this->load->model('record_model');

            $userSession = $this->session->get_userdata();
            $records = $this->record_model->getRecordUserById($userSession['logged_user']['0']['id'], $userSession['logged_user']['0']['company_id']);
                        
            $data = array('records' => $records);

            $this->load->view('header');
            $this->load->view('records', $data);
            $this->load->view('footer');
	}
        
        public function insertStart()
	{
            $this->output->set_content_type('application/json');
            
            $this->load->model('record_model');
            $userSession = $this->session->get_userdata();
            
            $record = array(
                'company_id' => $userSession['logged_user']['0']['company_id'],
                'user_id' => $userSession['logged_user']['0']['id'],
                'date_start' => date('y-m-d H:m:s')
            );
            
            if($this->record_model->createRecord($record) > 0){
                $this->output->set_output(json_encode('success'));
            } else {
                $this->output->set_output(json_encode('error'));
            }            				
	}
        
        public function updateRecord() {
            $this->output->set_content_type('application/json');
            
            $this->load->model('record_model');
            
            if (empty($_REQUEST['idRecord']) && empty($_REQUEST['type'])) {
                $this->output->set_output(json_encode('invalid id'));
                exit;
            }
            
                        
            if($_REQUEST['type'] == "end") {
                $field = "date_end";
            } else if ($_REQUEST['type'] == "lunch_start") { 
                $field = "date_lunch_start";
            } else {
                $field = "date_lunch_end";
            }           
            
            $record = array(
              'id' => $_REQUEST['idRecord'],
               $field => date('y-m-d H:m:s') 
            );
            
            if($this->record_model->setRecordByID($record, $field)){                
                $this->output->set_output(json_encode('success'));
            } else {
                $this->output->set_output(json_encode('error'));
            }            
        }
}