<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<http_request_method_name(method)e>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->model('user_model');
		$this->load->model('main_model');                
		$url = 'main';
                                
		$statistics = array(
                    'records' => $this->main_model->getCountRecords(),
                    'users' => $this->main_model->getCountUsers(),
                    'companies' => $this->main_model->getCountCompanies()
		);

		$data = array('statistics' => $statistics);
                
		$this->load->view('header');
		$this->load->view($url, $data);
		$this->load->view('footer');
	}
}
