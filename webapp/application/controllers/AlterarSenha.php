<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlterarSenha extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
            $this->load->model('user_model');
            $validationError = '';
            $validationStatus = '';

            if ($this->input->post('submit-alterar-senha-form') == 'submit') {
                $this->load->model('user_model');
                $userSession = $this->session->get_userdata();
                $user = array(
                    'name' => $userSession['logged_user']['0']['name'],
                    'password' => md5($this->input->post('actual-password'))
                );	

                $userInfo = $this->user_model->getUserByNameandPassword($user);
                if(!empty($userInfo)) {
                    $this->form_validation->set_rules('actual-password', 'Senha', 'required');
                    $this->form_validation->set_rules('new-password', 'Senha', 'required|min_length[6]|max_length[15]');
                    $this->form_validation->set_rules('confirm-new-password', 'Confirmação de senha', 'required|matches[new-password]');

                    if ($this->form_validation->run())
                    {				
                        $user = array(
                            'name' => $userSession['logged_user']['0']['name'],
                            'password' => md5($this->input->post('new-password'))
                        );
                        $this->user_model->updatePasswordByName($user);								
                        $validationStatus = 'success';
                    }
                    else
                    {
                        $validationError = validation_errors();
                        $validationStatus = 'error-validation';
                    }
                } else{
                    $validationStatus = 'incorrect-password';
                }
            }

            $userSession = $this->session->get_userdata();
            $userInfo = $this->user_model->getUserByID($userSession['logged_user']['0']['id']);
            $data = array('user' => $userInfo, 'validationError' => $validationError, 'validationStatus' => $validationStatus, 'title' => 'Alterar Senha');
            $this->load->view('header', $data);
            $this->load->view("alterar-senha", $data);
            $this->load->view('footer');
    }
}