<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */	
        
        public function index()
	{

	}

	public function formUserSignUpSubmit()
	{
            $this->output->set_content_type('application/json');
            $this->load->model('user_model');			
            $userName = $this->user_model->getUserByName($this->input->post('name'));

            if (empty($userName)) {			
                $this->form_validation->set_rules('name', 'Nome', 'required|min_length[6]');
                $this->form_validation->set_rules('password', 'Senha', 'required|min_length[6]|max_length[15]');
                $this->form_validation->set_rules('confirmPassword', 'Confirmação de senha', 'required|matches[password]');			
                $this->form_validation->set_rules('company_id', 'Empresa', 'required');			

                $success = $this->form_validation->run();                
                if ($success) {
                    $user = array(
                        'name' => $this->input->post('name'),
                        'password' => md5($this->input->post('password')),                        
                        'company_id' => $this->input->post('company_id')                        
                    );
                    
                    if($this->user_model->createUser($user) > 0){
                        $this->output->set_output(json_encode('submit-success'));
                    } else {
                        $this->output->set_output(json_encode('submit-error'));
                    }

                } else {                    
                    $this->output->set_output(json_encode(validation_errors()));
                }		
            } else {
                $this->output->set_output(json_encode('registered-name'));
            }
	}	


	public function formUserLoginSubmit()
	{		
		$this->output->set_content_type('application/json');
		$user = array(
                    'name' => $this->input->post('nameLogin'),
                    'password' => md5($this->input->post('passwordLogin'))
		);

		$this->load->model('user_model');
		$userInfo = $this->user_model->getUserByNameandPassword($user);		

		if (!empty($userInfo)) {
                    $this->session->set_userdata('logged_user', $userInfo);
                    $this->output->set_output(json_encode('Logado com sucesso!'));
		}else {
                    $this->output->set_output(json_encode('Não foi possível realizar o login! <br> Nome ou senha incorretos'));
		}
	}

	public function formUserLogout()
	{
		$this->output->set_content_type('application/json');
		//$this->output->enable_profiler(TRUE);
		$this->session->unset_userdata('logged_user');
		$this->output->set_output(json_encode('success'));
	}

}