# Solides WebApp

Aplica��o desenvolvida no framework PHP CodeIgniter com banco de dados MySQL. Utilizado dos conceitos e bibliotecas do Bootstrap e jQuery.

A aplica��o consiste em um simples sistema para funcion�rios apontarem o hor�rio de trabalho nos momentos de: Chegada, Intervalos para Almo�o e Sa�da. Apresenta ainda o hist�rico dos apontamentos.


### Configura��es a serem feitas

## Alterar $config['base_url']:

```
aplication\config\config.php
```

## Alterar as configura��es de hostname, username, password e database:

```
aplication\config\database.php
```

## Importa��o do banco de dados (DUMP MySQL):

```
DUMP\solides_webapp.sql
```

### Funcionalidades finalizadas

- Tela de Login
- Tela de Cadastro
- Tela de hist�rico de apontamentos do usu�rio

### Funcionalidades pendentes

- Controle completo da sess�o (a mesma est� startada, por�m n�o completamente validada em todo o sistema).
- Tela para apontar a hora inabacada. Est� funcionando somente a parte da chegada. E faltou ainda aprimorar o layout dela e validar.
- Tela para cadastrar empresa (n�o foi solicitado, por�m inseri no projeto por achar pertinente).

### Erros/Bugs

- Tela de listagem usu�rios � inexistente.
- A barra do menu principal est� com um defeito de posicionamento.
- No cadastro de usu�rio s� permitir� sempre selecionar a mesma empresa (est� no DUMP a inser��o inicial) porque n�o foi feito a tela de cadastro.